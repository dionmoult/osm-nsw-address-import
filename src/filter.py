#!/usr/bin/python3

# On my machine, gdal does not load in a pyvenv.
import sys
sys.path.append('/usr/lib64/python3.6/site-packages')

import geojson
import ogr
import os
from itertools import filterfalse

input_dir = './raw_bb_extract/'
output_dir = './processed_bb_extract/'

for filename in os.listdir(input_dir):
    suburb = filename.split('/')[-1].split('.geojs')[0].replace('-', ' ')

    # http://maps.six.nsw.gov.au/sws/AddressLocation.html
    road_names = [
        'ACCESS', 'ALLEY', 'ALLEYWAY',
        'AMBLE', 'APPROACH', 'ARCADE',
        'ARTERIAL', 'ARTERY', 'AVENUE',
        'BANAN', 'BEND', 'BOARDWALK',
        'BOULEVARD', 'BRACE', 'BRAE',
        'BREAK', 'BROADWAY', 'BROW',
        'BYPASS', 'BYWAY', 'CAUSEWAY',
        'CENTRE', 'CHASE', 'CIRCLE',
        'CIRCLET', 'CIRCUIT', 'CIRCUS',
        'CLOSE', 'COMMON', 'CONCOURSE',
        'COPSE', 'CORNER', 'COURT',
        'COURTYARD', 'COVE', 'CRESCENT',
        'CREST', 'CROSS', 'CROSSING',
        'CUL-DE-SAC', 'CUTTING', 'DALE',
        'DEVIATION', 'DIP', 'DISTRIBUTOR',
        'DRIVE', 'DRIVEWAY', 'EDGE',
        'ELBOW', 'END', 'ENTRANCE',
        'ESPLANADE', 'EXPRESSWAY', 'EXTENSION',
        'FAIRWAY', 'FIRETRACK', 'FIRETRAIL',
        'FOLLOW', 'FOOTWAY', 'FORMATION',
        'FREEWAY', 'FRONTAGE', 'GAP',
        'GARDEN', 'GARDENS', 'GATE',
        'GLADE', 'GLEN', 'GRANGE',
        'GREEN', 'GROVE', 'HEIGHTS',
        'HIGHROAD', 'HIGHWAY', 'HILL',
        'INTERCHANGE', 'JUNCTION', 'KEY',
        'LANE', 'LANEWAY', 'LINE',
        'LINK', 'LOOKOUT', 'LOOP',
        'MALL', 'MEANDER', 'MEWS',
        'MOTORWAY', 'NOOK', 'OUTLOOK',
        'PARADE', 'PARKWAY', 'PASS',
        'PASSAGE', 'PATH', 'PATHWAY',
        'PIAZZA', 'PLACE', 'PLAZA',
        'POCKET', 'POINT', 'PORT',
        'PROMENADE', 'QUADRANT', 'QUAY',
        'QUAYS', 'RAMBLE', 'RAMP',
        'REST', 'RETREAT', 'RIDGE',
        'RING', 'RISE', 'ROAD',
        'ROADS', 'ROTARY', 'ROUTE',
        'ROW', 'RUE', 'SERVICEWAY',
        'SHUNT', 'SPUR', 'SQUARE',
        'STAIRS', 'STEPS', 'STREET',
        'STRIP', 'SUBWAY', 'TARN',
        'TERRACE', 'THOROUGHFARE', 'TOLLWAY',
        'TOP', 'TOR', 'TRACK',
        'TRAIL', 'TURN', 'UNDERPASS',
        'VALE', 'VIADUCT', 'VIEW',
        'VISTA', 'WALK', 'WALKWAY',
        'WAY', 'WHARF', 'WYND',
        # Some custom ones I've found
        'CLOISTERS', 'BASTION', 'RAMPART', 'SANCTUARY', 'RUN', 'BUNDARRA',
        'REACH', 'HERMITAGE'
    ]

    def is_address_outside_suburb(feature):
        if 'address' not in feature['properties'] \
            or 'housenumber' not in feature['properties']:
            return True
        address = feature['properties']['address']
        if address[-len(suburb):] == suburb \
            and address[0:-len(suburb)-1].split(' ')[-1] in road_names:
            return False
        # The NORTH SOUTH EAST WEST trigger too many false positives
        if address[-len(suburb):] == suburb \
            and address[0:-len(suburb)-1].split(' ')[-1] not in ['NORTH', 'SOUTH', 'EAST', 'WEST']:
            print('Is this address really not in {}? {}'.format(suburb, address))
        return True

    with open(input_dir + filename) as file:
        data = geojson.loads(file.read())
        data['features'][:] = filterfalse(is_address_outside_suburb, data['features'])
        for feature in data['features']:
            geometry = ogr.CreateGeometryFromJson(geojson.dumps(feature['geometry']))
            feature['geometry'] = geojson.loads(geometry.Centroid().ExportToJson())
            properties = feature['properties']
            properties['address']
            properties['addr:housenumber'] = properties['housenumber']
            properties['addr:street'] = properties['address'][len(properties['addr:housenumber'])+1:-len(suburb)-1].title()
            properties.pop('RID')
            properties.pop('propid')
            properties.pop('urbanity')
            properties.pop('housenumber')
            properties.pop('address')
        with open(output_dir + filename, 'w') as outfile:
            outfile.write(geojson.dumps(data))
