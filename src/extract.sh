#!/bin/bash

# Usage:
#
# ## Automagic processing!
#
# ./extract.sh all "SUBURB NAME"
#
# ## Step by step instructions for manual processing
#
# Get a page of 500 results between xmin, ymin (bottom left) and xmax, ymax
# (top right). First offset by 0, then by 500 for the next page, then by 2000,
# etc.
#
# $ ./extract.sh extract 151.08188 -33.76661 151.10773 -33.75052 0
# $ ./extract.sh extract 151.08188 -33.76661 151.10773 -33.75052 500
# $ ./extract.sh extract 151.08188 -33.76661 151.10773 -33.75052 1000
#
# Check the resulting input*.json to see if you are on the last page. Then
# convert it from ArcGIS JSON to GeoJSON and merge into merged.geojson. If you
# haven't already done `npm install`, do it to get the geojson-merge tool.
#
# $ ./extract.sh convert-merge
#
# Clean up WIP files.
#
# $ ./extract.sh clean

if [ "$1" == 'all' ]; then
    suburb_coordinates=`grep -i "^$2," bounding_boxes_of_suburbs.csv`
    offset=0
    IFS=, read name postcode urbanity xmin ymin xmax ymax <<< "$suburb_coordinates"
    read -p "Is this the correct suburb and postcode? ($name $postcode) [Y/n]: " yn
    if [ "$yn" == 'n' ]; then exit; fi
    while true; do
        echo "Starting download at offset $offset ..."
        while true; do
            ./extract.sh extract $xmin $ymin $xmax $ymax $offset
            filesize=`wc -c < input$offset.json`
            if (( "$filesize" >= 803 )); then
                echo "Download successful ..."
                break
            else
                echo "Download failed, trying again ..."
            fi
        done
        if [ $filesize == 803 ]; then
            echo "There is no more to download ..."
            break
        fi
        offset=$((offset+500))
    done
    echo "Merging files ..."
    ./extract.sh convert-merge
    mv merged.geojson "$name,$postcode.geojson"
    echo "Cleaning temporary files ..."
    ./extract.sh clean
    echo "Running clean data script ..."
    ./clean_data.py "$name" "$name,$postcode.geojson" "../cleaned/$name,$postcode.osm"
    echo "Removing work in progress geojson file ..."
    rm "$name,$postcode.geojson"
    echo "All done! Review your results in the cleaned/ directory!"
fi

if [ "$1" == 'extract' ]; then
    curl -m 30 http://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Property/MapServer/4/query\?where\=1%3D1\&text\=\&objectIds\=\&time\=\&geometry\=%7Bxmin%3A+$2%2C+ymin%3A+$3%2C+xmax%3A+$4%2C+ymax%3A+$5%2C+spatialReference+%3A+%7Bwkid+%3A+4326%7D%7D\&geometryType\=esriGeometryEnvelope\&inSR\=\&spatialRel\=esriSpatialRelContains\&relationParam\=\&outFields\=\*\&returnGeometry\=true\&returnTrueCurves\=false\&maxAllowableOffset\=\&geometryPrecision\=9\&outSR\=4326\&returnIdsOnly\=false\&returnCountOnly\=false\&orderByFields\=\&groupByFieldsForStatistics\=\&outStatistics\=\&returnZ\=false\&returnM\=false\&gdbVersion\=\&returnDistinctValues\=false\&resultOffset\=$6\&resultRecordCount\=500\&f\=pjson > input$6.json
fi

if [ "$1" == 'convert-merge' ]; then
    for f in input*; do
        ogr2ogr -f GeoJSON output-$f.geojson $f
    done
    ./node_modules/@mapbox/geojson-merge/geojson-merge output* > merged.geojson
fi

if [ "$1" == 'clean' ]; then
    rm -f input*
    rm -f output*
    rm -f merged*
fi
