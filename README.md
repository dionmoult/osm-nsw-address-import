# NOTICE: This import is currently paused due to a misunderstanding about which NSW LPI endpoints we have permission to use.

We hope to resolve this soon!

# osm-nsw-address-import

This is the code repository for import data and scripts related to the [Australia NSW Property and Address Import](https://wiki.openstreetmap.org/wiki/Australia_NSW_Property_and_Address_Import) for Open Street Map.

*  data extraction code is located in `src`
*  `*.osm` files awaiting review belong in the `/review` folder
*  `*.osm` files should be moved from the `/review` folder to the `/uploaded` folder once they have been uploaded to OSM

## Dependencies
Before you start make sure you have the following dependencies installed:

### Required Dependencies

 * `gdal` for ogr2ogr
 * `npm`
 * `python3-pip`

To install dependencies:

```
$ cd src/ && npm install && pip install -r requirements.txt
```

### Optional Dependencies

After each suburb is added to the `review/` directory, it should be deleted from `src/bounding_boxes_of_suburbs.csv` (details in subsequent section). This step is easy to forget. You can install a `git` pre-commit hook to automatically remove this line based on the names of suburbs added to the `review/` directory in the latest commit as follows:

    cd .git/hooks
    ln -s ../../src/git-hooks/pre-commit ./pre-commit

This will run whenever you run `git commit`.

## How to import

There are three parts to the import process. The import process is broken up
into suburb by suburb, for ease of manual review. In short:

 1. Download a dump of all addresses in a suburb.
 2. Clean that dump for that suburb and isolate it into two parts: one ready to
    upload, *awaiting review*, and one that needs a *manual ground survey review*.
 3. Peer review those that are ready to upload, and after two weeks with no
    objections, upload it to OSM!

See a detailed break-down on how to contribute to each step below.

## Step 1: Download a suburb data dump

I have created a CSV file called `src/bounding_boxes_of_suburbs.csv` which
contains a list of every single suburb in NSW, and the calculated rectangular
bounding box of each suburb. If you look inside, you will see the name of each
suburb that is left to import. Here is an example line:

    HARRIS PARK,2150,U,151.0070151,-33.8276787,151.0168683,-33.8178911

Choose a suburb that you'd like to download, and run:

    cd src/ && ./extract.sh all "HARRIS PARK"

If the suburb name is ambiguous, you can also specify the postcode as follows:

    ./extract.sh all "HARRIS PARK,2150"

Replace `HARRIS PARK` with the name of the suburb that you'd like to extract.
Follow the prompts, and when complete you will have a file in `cleaned/HARRIS PARK,2150.osm` which you can use.

Finally, delete the line in `src/bounding_boxes_of_suburbs.csv` so that we know
that it has already been downloaded.

## Step 2: Clean a suburb data dump

![JOSM with Harris Park centroids](img/josm-harris-park-centroid.png)

Now, use [JOSM](https://josm.openstreetmap.de/) to open up the `osm` file. To
start with, perform a data validation using `Shift-V`. It is likely that you
will get three types of validation errors.

  1. "Duplicate House number"
  2. "Nodes at same position" 
  3. "Other duplicated nodes"

Note that sometimes not all errors are present in one suburb.

Start with the third validation error "Other duplicated nodes". These are simply duplicates and you can press "Fix" to have JOSM fix them immediately. (Do this first as it might clean up some of the other errors. Re-run data validation afterwards again)

The first validation error "Duplicate House number" can be resolved by deleting one of the duplicate numbers manually. Moving of nodes should be done before something is deleted to check if there is another node underneath.

Now, there are also "Nodes at the same position". These nodes account for e.g. different units within the same building.
If you know from ground survey where the numbers belong you can move them accordingly.

If not, the policy right now is that if there are 2 or 3 nodes in the same position, just displace them
slightly apart so that both human mappers can see them, and routing will still
work.

To fix the 2 / 3 nodes in the same position scenario, select all 2 / 3 node
errors in the validation panel in JOSM, and go into `Scripting -> Show scripting
console` and load `src/displace_overlapping_nodes.js`, and run it. You will find
that all those nodes are displaced.
(NOTE: You will need to install the `Scripting` plugin, and restart JOSM before
doing this.)

If there are 4 or more nodes, or other validation errors, move the respective nodes to a separate new layer and save the layer as file `FOR-MANUAL-REVIEW-<SUBURB,POSTCODE>.osm`, where `<SUBURB,POSTCODE>` is replaced with the suburb name and postcode.
e.g. `review/FOR-MANUAL-REVIEW-HARRIS PARK,2150.osm`
A human mapper will review these locations on the ground and we'll figure out how to best tag these locations appropriately later manually.

For the remainder, make sure there are no more validation errors by re-running
the validation. Then, go and download the OSM map data for that area in another layer,
so you can start doing a human manual review. 
It is important to run the validation when all nodes are visible or using the keyboard shortcut `Shift-V` else you run the risk to not cover all nodes.

### Human Manual Review

For starters, spot any already added addresses, and delete them in our data set
so we don't double up on addresses. For this, try using the
`src/elemstyles.mapcss` map styling in JOSM, which will highlight in a bright
red anything which has an address. You may also consider [learning to use the
conflation plugin](./Conflation.md), which will also help with merging existing buildings with addresses.

### Upload to GitLab for review

After you have finished your manual review, save the updated
`review/FOR-MANUAL-REVIEW-<SUBURB,POSTCODE>.osm` and your newly created and reviewed
dataset at `review/<SUBURB,POSTCODE>.osm`.

To be completely clear, `SUBURB,POSTCODE.osm` is the file created by the automatic extraction, minus the nodes which were moved into `FOR-MANUAL-REVIEW-SUBURB,POSTCODE.osm`. At the end of this process, both files should be in the `review/` directory, and neither should be in the `cleaned/` directory.

Commit the changes and then create a merge request to this
repository so that other people in the OSM community can do a peer review.

## Step 3: Peer review and upload

### Peer Review

Check out the `review/` directory for things to review. Files named `SUBURB,POSTCODE.osm` which are greater than two weeks old, with no [outstanding issues](https://gitlab.com/dionmoult/osm-nsw-address-import/issues) and no ongoing debate on talk-au are good to upload to OSM. (Make sure to quickly review it yourself before uploading.) Feel free to review other people's datasets.

Files named `FOR-MANUAL-REVIEW-SUBURB,POSTCODE.osm` may require a ground survey, or additional verification from other (OSM friendly) sources to clarify the existance and exact position of nodes.

### Uploading to OSM

When uploading, you may use your own OSM account but ensure propper tagging.

In order to provide good and correct attribution to the import project and the data source the following tags shall be added during the upload to OSM:

```
comment=Upload addresses for <Suburb> (Australia NSW Property and Address Import)
source=LPI NSW Imagery; LPI NSW Base Map
source:import=NSW LPI Web Services
import:wiki=https://wiki.openstreetmap.org/wiki/Australia_NSW_Property_and_Address_Import
import:repository=https://gitlab.com/dionmoult/osm-nsw-address-import
```

*  You can copy the whole block of tags above and automatically add them to the changeset in JOSM with one click.
   Just select the "Tags of new changeset tab", and click the button with three '+' signs.
*  Don't forget to change the suburb from `<Suburb>` to the actual suburb name.

![JOSM changeset tags prior to uploading](img/josm_import_tags.png "JOSM changeset tags prior to uploading")

Changeset Examples:

*  [early changeset](https://www.openstreetmap.org/changeset/62356201)
*  [older properly tagged changeset](https://www.openstreetmap.org/changeset/72987679)
*  [changeset with current tagging practice](https://www.openstreetmap.org/changeset/73286869)

Correctly tagged [OSM Changesets of the last 30 Days filtered by 'NSW LPI Web Services'](http://resultmaps.neis-one.org/osm-changesets?comment=NSW%20LPI%20Web%20Services#9/-34.1936/150.6967)

It is vital to tag it with source so we can identify it later. This should work both ways (from this repo to osm and from osm back to this repo).

### Cleaning up after uploading to OSM

After uploading, please move the uploaded file from the `review` directory to the `uploaded` directory,
and link to the changeset in the git commit message. For example,

```
git mv review/SUBURB,POSTCODE.osm uploaded/
git add uploaded/SUBURB,POSTCODE.osm
git commit -m "Upload <Suburb> <link to changeset>"
```

### More to do

In addition, you can check out the points in the `review/FOR-MANUAL-REVIEW-<SUBURB,POSTCODE>.osm` files.
These need completely manual review, please have fun checking out these
locations in real life and mapping them, and feel free to ask in the mailing list talk-au for help.

Once done, just remove those points from the `review/FOR-MANUAL-REVIEW-<SUBURB>.osm`
file. (Again, create a merge request to this repository so that we can keep track of everything!)
